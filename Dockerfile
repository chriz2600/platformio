FROM debian:12-slim

ARG VERSION="6.1.15"
RUN apt-get update && \
    apt-get -y install git python3 python3-pip locales curl nodejs xxd && \
    curl -sL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get -y install nodejs build-essential && \
    npm install -g inliner && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen en_US.UTF-8 && \
    update-locale LANG=en_US.UTF-8 && \
    pip install --break-system-packages --no-cache-dir -U platformio$VERSION && \
    apt-get -y autoremove && \
    rm -rf /var/lib/apt/lists/*
